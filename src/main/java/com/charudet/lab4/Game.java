/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.charudet.lab4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {
    private Player player_X, player_O;
    private Table table;
    
    public Game() {
        player_X = new Player('x');
        player_O = new Player('o');
    }
    
    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while(!isFinish){           
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()){
                printTable();
                printWinner();
                printPlayer();
                isFinish = true;
            }
            if (table.checkDraw()){
                printTable();
                printDraw();
                printPlayer();
                isFinish = true;
            }         
            table.switchPlayer();
        }
    }
    
    private void printWelcome(){
        System.out.println("Tic-Tac-Toe!");
    }
    
    private void printTable(){
        System.out.println("-------------");
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " | ");
            }
            System.out.println(" ");
            System.out.println("-------------");
        }
    }
    
    private void printTurn(){
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    
    private void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }
    
    private void newGame(){
        table = new Table(player_X, player_O);
    }
    
    private void printWinner(){
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!!");
    }
    
    private void printDraw(){
        System.out.println("Draw!");
    }
    
    private void printPlayer() {
        System.out.println(player_X);
        System.out.println(player_O);
    }
}