/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.charudet.lab4;

/**
 *
 * @author User
 */
public class Table {
    private char [][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player_X, player_O, currentPlayer;
    private int row, col;
    private int count;

    public Table(Player player_X, Player player_O) {
        this.player_X = player_X;
        this.player_O = player_O;
        this.currentPlayer = player_X;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public boolean setRowCol(int row, int col) {
        if(table[row-1][col-1] == '-') {
            table[row-1][col-1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            this.count++;
            return true;
        }
        return false;
    }
    
    public boolean checkWin() {
        if(checkRow()) {
            saveWin();
            return true;
        }
        if(checkCol()) {
            saveWin();
            return true;
        }
        if(checkDiagonal1()) {
            saveWin();
            return true;
        }
        if(checkDiagonal2()) {
            saveWin();
            return true;
        }
        return false;
    }
    
    private boolean checkRow() {
        return table[row-1][0] != '-' && table[row-1][0] == table[row-1][1] && table[row-1][0] == table[row-1][2];
    }
    
    private boolean checkCol() {
        return table[0][col-1] != '-' && table[0][col-1] == table[1][col-1] && table[0][col-1] == table[2][col-1];
    }
    
    private boolean checkDiagonal1() {
        //return false;
        return table[0][0] != '-' && table[0][0] == table[1][1] && table[0][0] == table[2][2];
    }
    
    private boolean checkDiagonal2() {
        //return false;
        return table[0][2] != '-' && table[0][2] == table[1][1] && table[0][2] == table[2][0];
    }
    
    public boolean checkDraw() {
        if(count == 9) {
            player_X.draw();
            player_O.draw();
            return true;
        }
        return false;
    }
    
    private void saveWin() {
        if(currentPlayer == player_X) {
            player_X.win();
            player_O.lost();
        } else {
            player_O.win();
            player_X.lost();
        }
    }

    void switchPlayer() {
        if (currentPlayer == player_X) {
           currentPlayer = player_O;
        } else {
            currentPlayer = player_X;
        }

    }
    
}
