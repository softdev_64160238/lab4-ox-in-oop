/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.charudet.lab4;

/**
 *
 * @author User
 */
public class Player {
    private char symbol;
    private int winCount, lostCount, drawCount;
    
    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLostCount() {
        return lostCount;
    }

    public int getDrawCount() {
        return drawCount;
    }
    
    public void win() {
        winCount++;
    }
    
    public void lost() {
        lostCount++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", lostCount=" + lostCount + ", drawCount=" + drawCount + '}';
    }
    
    public void draw() {
        drawCount++;
    }
    
}
